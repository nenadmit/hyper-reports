package configuration;

import deserialization.XmlParser;
import java.io.File;

public class AppConfigurer {

    private static AppConfiguration configuration;
    private static File configFile;

    private AppConfigurer() {

    }

    public static AppConfiguration getConfiguration() {

        File file = new File("Configuration.xml");

        if (configuration == null) {
            configuration = new XmlParser<AppConfiguration>(AppConfiguration.class).parse(file);
            configFile = file;
        }

        return configuration;
    }

    public static File getConfigFile() {
        return configFile;
    }
}
