package report_engine.reports;

public class ReportConstants {

    public static final String MONTHLY_REPORT = "MONTHLY_REPORT";
    public static final String QUARTERLY_REPORT = "QUARTERLY_REPORT";
    public static final String YEARLY_REPORT = "YEARLY_REPORT";
    public static final String CARD_AGGREGATION = "card";
    public static final String CASH_AGGREGATION = "cash";
    public static final String STORE_AGGREGATION = "store";
    public static final String INVOICE_AGGREGATION = "invoice";
    public static final String RECEIPT_AGGREGATION = "receipt";
    public static final String PAYMENT_AGGREGATION = "payment";

    private ReportConstants(){
    }
}
