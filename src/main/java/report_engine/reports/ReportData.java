package report_engine.reports;

import java.util.Map;

public class ReportData implements Comparable<ReportData> {


    private String aggregationName;
    private Map<String, Double> turnoverPerTimePeriod;
    private double total;

    public String getAggregationName() {
        return aggregationName;
    }

    protected void setAggregationName(String aggregationName) {
        this.aggregationName = aggregationName;
    }

    public Map<String, Double> getTurnoverPerTimePeriod() {
        return turnoverPerTimePeriod;
    }

    protected void setTurnoverPerTimePeriod(Map<String, Double> turnoverPerTimePeriod) {
        this.turnoverPerTimePeriod = turnoverPerTimePeriod;
    }

    public double getTotal() {
        return total;
    }

    protected void setTotal(double total) {

        this.total = total;
    }

    @Override
    public int compareTo(ReportData o) {
        return Double.compare(getTotal(),o.getTotal());
    }

    @Override
    public String toString() {
        return "ReportData{" +
                "aggregationName='" + aggregationName + '\'' +
                ", turnoverPerTimePeriod=" + turnoverPerTimePeriod +
                ", total=" + total +
                '}';
    }
}
