package report_engine.reports;

import deserialization.pojo.company.Invoice;
import deserialization.pojo.company.Receipt;
import org.exolab.castor.types.DateTime;
import java.time.Month;
import java.util.*;
import java.util.logging.Logger;

public class ReportDataProcessor {

    private final String reportType;

    public ReportDataProcessor(String reportType) {
        this.reportType = reportType;
    }

    public ReportData getReport(List<Invoice> invoices,String aggregationName){
        return getReport(aggregationName,invoices,new ArrayList<>());
    }

    public ReportData getReport(String aggregationName,List<Receipt> receipts){
        return getReport(aggregationName,new ArrayList<>(),receipts);
    }


    public ReportData getReport(String aggregationName,List<Invoice> invoices, List<Receipt> receipts) {

        Map<String, Double> dataMap = new LinkedHashMap<>();
        ReportData reportData = new ReportData();

        for (Invoice invoice : invoices) {
            String quarter = getTimePeriod(invoice.getDatetime());
            if (!dataMap.containsKey(quarter)) {
                dataMap.put(quarter, invoice.getTotal());
            } else {
                dataMap.put(quarter, dataMap.get(quarter) + invoice.getTotal());
            }
        }
        for (Receipt receipt : receipts) {
            String quarter = getTimePeriod(receipt.getDatetime());
            if (!dataMap.containsKey(quarter)) {
                dataMap.put(quarter, receipt.getTotal());
            } else {
                dataMap.put(quarter, dataMap.get(quarter) + receipt.getTotal());
            }
        }
        reportData.setAggregationName(aggregationName);
        reportData.setTurnoverPerTimePeriod(dataMap);
        reportData.setTotal(calculateTotal(dataMap.values()));

        return reportData;
    }


    private String getTimePeriod(Date date) {

        if (reportType.equals(ReportConstants.MONTHLY_REPORT)) {
            return String.valueOf(new DateTime(date).getDay());

        } else if (reportType.equals(ReportConstants.QUARTERLY_REPORT)) {
            return Month.of(new DateTime(date).getMonth()).name();

        } else if (reportType.equals(ReportConstants.YEARLY_REPORT)) {
            return getQuarter(new DateTime(date).getMonth());

        } else {
            throw new IllegalArgumentException("Unknown report type" + reportType);
        }
    }

    private String getQuarter(int month) {

        if (month > 0 && month <= 3) {
            return "QUARTER 1";

        } else if (month > 3 && month <= 6) {
            return "QUARTER 2";

        } else if (month > 6 && month <= 9) {
            return "QUARTER 3";

        } else if (month > 9 && month <= 12) {
            return "QUARTER 4";

        } else {
            throw new IllegalArgumentException("month cannot be less than 0 or greather than 12");
        }
    }

    private double calculateTotal(Collection<Double> turnovers) {

        double turnover = 0;
        for (Double total : turnovers) {
            turnover  = turnover + total;
        }
        return turnover;
    }
}
