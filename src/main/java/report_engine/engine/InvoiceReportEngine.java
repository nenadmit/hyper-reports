package report_engine.engine;

import database.CompanyDataProcessor;
import deserialization.pojo.company.Company;
import deserialization.pojo.company.Invoice;
import report_engine.reports.ReportData;
import report_engine.reports.ReportConstants;
import report_engine.reports.ReportDataProcessor;
import java.util.Arrays;
import java.util.List;

public class InvoiceReportEngine implements ReportEngine {

    private CompanyDataProcessor processor;
    private ReportDataProcessor reportProcessor;
    private static final String AGGREGATION_NAME = "INVOICES";

    public InvoiceReportEngine(CompanyDataProcessor processor) {
        this.processor = processor;
    }

    @Override
    public List<ReportData> createMonthlyReport(Company company, int month, int year, boolean desc, int limit) {

        List<Invoice> invoices = processor.getInvoiceService().findAllByCompanyAndMonth(company.getId(), month, year,"ALL");
        reportProcessor = new ReportDataProcessor(ReportConstants.MONTHLY_REPORT);

        return Arrays.asList(reportProcessor.getReport(invoices,
                AGGREGATION_NAME));
    }

    @Override
    public List<ReportData> createQuarterlyReport(Company company, int quarter, int year, boolean desc, int limit) {

        List<Invoice> invoices = processor.getInvoiceService().findAllByCompanyAndQuarter(company.getId(), quarter, year,"ALL");
        reportProcessor = new ReportDataProcessor(ReportConstants.QUARTERLY_REPORT);

        return Arrays.asList(reportProcessor.getReport(invoices,
                AGGREGATION_NAME));
    }

    @Override
    public List<ReportData> createYearlyReport(Company company, int year, boolean desc, int limit) {

        List<Invoice> invoices = processor.getInvoiceService().findAllByCompanyAndYear(company.getId(), year,"ALL");
        reportProcessor = new ReportDataProcessor(ReportConstants.YEARLY_REPORT);

        return Arrays.asList(reportProcessor.getReport(invoices,
                AGGREGATION_NAME));
    }
}
