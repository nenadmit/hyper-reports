package report_engine.engine;

import database.CompanyDataProcessor;
import deserialization.pojo.company.Company;
import deserialization.pojo.company.Receipt;
import report_engine.reports.ReportData;
import report_engine.reports.ReportConstants;
import report_engine.reports.ReportDataProcessor;

import java.util.Arrays;
import java.util.List;

public class ReceiptReportEngine implements ReportEngine {

    private CompanyDataProcessor processor;
    private ReportDataProcessor reportProcessor;
    private static final String AGGREGATION_NAME = "RECEIPTS";

    public ReceiptReportEngine(CompanyDataProcessor processor) {
        this.processor = processor;
    }

    @Override
    public List<ReportData> createMonthlyReport(Company company, int month, int year, boolean desc, int limit) {

        List<Receipt> receipts = processor.getReceiptService().findAllByCompanyAndMonth(company.getId(), month, year,"ALL");
        reportProcessor = new ReportDataProcessor(ReportConstants.MONTHLY_REPORT);

        return Arrays.asList(reportProcessor.getReport(AGGREGATION_NAME,
                receipts));
    }

    @Override
    public List<ReportData> createQuarterlyReport(Company company, int quarter, int year, boolean desc, int limit) {

        List<Receipt> receipts = processor.getReceiptService().findAllByCompanyAndQuarter(company.getId(), quarter, year,"ALL");
        reportProcessor = new ReportDataProcessor(ReportConstants.QUARTERLY_REPORT);

        return Arrays.asList(reportProcessor.getReport(AGGREGATION_NAME,
                receipts));
    }

    @Override
    public List<ReportData> createYearlyReport(Company company, int year, boolean desc, int limit) {

        List<Receipt> receipts = processor.getReceiptService().findAllByCompanyAndYear(company.getId(), year,"ALL");
        reportProcessor = new ReportDataProcessor(ReportConstants.YEARLY_REPORT);

        return Arrays.asList(reportProcessor.getReport(AGGREGATION_NAME,
                receipts));
    }
}
