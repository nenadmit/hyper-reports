package jcommander.service;

import com.beust.jcommander.ParameterException;
import configuration.AppConfigurer;
import database.CompanyDataProcessor;
import deserialization.pojo.company.Company;
import jcommander.CommandLineParameters;
import report_engine.XlsxParser;
import report_engine.engine.*;
import report_engine.reports.ReportConstants;
import report_engine.reports.ReportData;

import java.io.IOException;
import java.time.Month;
import java.util.LinkedList;
import java.util.List;

public class ReportCreator {

    private CommandLineParameters parameters;
    private CompanyDataProcessor processor;
    private XlsxParser xlsxParser;

    public ReportCreator(CommandLineParameters parameters, CompanyDataProcessor processor, XlsxParser xlsxParser) {
        this.parameters = parameters;
        this.processor = processor;
        this.xlsxParser = xlsxParser;
    }

    public void createReport() {

        String extension = ".xlsx";
        String exportDirectory = AppConfigurer.getConfiguration().getDataDirectories().getExportDir();
        if (exportDirectory.charAt(exportDirectory.length() - 1) != '/') {
            exportDirectory = exportDirectory + "/";
        }
        List<ReportData> reportData = prepareReportData(processor);
        xlsxParser.exportAsXlsx(reportData, exportDirectory + getFileName() + extension);
        System.out.println("Report created in " +exportDirectory + getFileName());

    }

    private String getReportType() {

        if (parameters.getMonths() != 0 && parameters.getQuarter() == null) {
            return ReportConstants.MONTHLY_REPORT;
        } else if (parameters.getMonths() == 0 && parameters.getQuarter() != null) {
            return ReportConstants.QUARTERLY_REPORT;
        } else if (parameters.getMonths() != 0 && parameters.getQuarter() != null) {
            throw new ParameterException("Both --month and --quarter parameters cannot be present as the same time.");
        }
        return ReportConstants.YEARLY_REPORT;
    }

    private ReportEngine getReportEngine(CompanyDataProcessor processor) {

        String aggregation = parameters.getAggregation();
        aggregation = aggregation.toLowerCase();

        switch (aggregation) {
            case ReportConstants.STORE_AGGREGATION:
                return new StoreReportEngine(processor);
            case ReportConstants.INVOICE_AGGREGATION:
                return new InvoiceReportEngine(processor);
            case ReportConstants.RECEIPT_AGGREGATION:
                return new ReceiptReportEngine(processor);
            case ReportConstants.PAYMENT_AGGREGATION:
                return new PaymentReportEngine(processor);
            default: {
                throw new IllegalArgumentException("Unsupported aggregation type, " + aggregation);
            }
        }
    }

    private List<ReportData> prepareReportData(CompanyDataProcessor processor) {

        List<ReportData> reportData = new LinkedList<>();
        ReportEngine engine = getReportEngine(processor);
        String reportType = getReportType();

        Company company = processor.getCompanyService().findByName(parameters.getCompanyName()).get();

        boolean order = false;
        if (parameters.getOrder() != null) {
            order = getOrderAsBool(parameters.getOrder());
        }
        switch (reportType) {
            case ReportConstants.MONTHLY_REPORT: {
                reportData = engine.createMonthlyReport(company, parameters.getMonths(),
                        parameters.getYear(),
                        order,
                        parameters.getTop());
                break;
            }
            case ReportConstants.QUARTERLY_REPORT: {
                reportData = engine.createQuarterlyReport(company,
                        getQuarterAsInt(parameters.getQuarter()),
                        parameters.getYear(),
                        order,
                        parameters.getTop());
                break;
            }
            case ReportConstants.YEARLY_REPORT: {
                reportData = engine.createYearlyReport(company, parameters.getYear(),
                        order,
                        parameters.getTop());
                break;
            }

        }
        return reportData;

    }

    private String getFileName() {

        String reportType = getReportType();
        String companyName = parameters.getCompanyName();
        int month = parameters.getMonths();
        int year = parameters.getYear();
        String quarter = parameters.getQuarter();
        String aggregationType = parameters.getAggregation();

        switch (reportType) {
            case ReportConstants.MONTHLY_REPORT: {
                String monthName = Month.of(month).name().toLowerCase();
                return String.format("report-%s-%s-%s-%s", companyName, monthName, year, aggregationType);
            }
            case ReportConstants.QUARTERLY_REPORT: {
                return String.format("report-%s-%s-%s-%s", companyName, quarter, year, aggregationType);
            }
            case ReportConstants.YEARLY_REPORT: {
                return String.format("report-%s-%s-%s", companyName, year, aggregationType);
            }
            default: {
                throw new IllegalArgumentException("Unrecognized reportType " + reportType);
            }
        }
    }

    private int getQuarterAsInt(String quarter) {
        return Integer.valueOf(quarter.substring(quarter.length() - 1));
    }

    private boolean getOrderAsBool(String order) {
        return order.equals("desc");
    }

}
