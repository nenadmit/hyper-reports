package jcommander.service;

import com.beust.jcommander.ParameterException;
import configuration.AppConfiguration;
import configuration.AppConfigurer;
import database.CompanyDataProcessor;
import jcommander.CommandLineParameters;
import report_engine.reports.ReportConstants;

public class ParameterValidator {

    private CommandLineParameters parameters;
    private AppConfiguration configuration;
    private CompanyDataProcessor processor;

    public ParameterValidator(CommandLineParameters parameters, CompanyDataProcessor processor) {
        this.processor = processor;
        this.parameters = parameters;
        configuration = AppConfigurer.getConfiguration();
    }


    public void validateParameter() {

        validateReportParameters();
        validateAggregationType();
        doesCompanyExist(processor);


    }

    public String validateImportDirectory() {

        if (configuration.getDataDirectories().getImportDir().isEmpty()) {
            throw new ParameterException("Import directory cannot be empty \\n" +
                    " Set data directory using command --data-dir || -dd <importDirectoryPath>");
        }
        return configuration.getDataDirectories().getImportDir();
    }

    public String validateExportDirectory() {
        if (configuration.getDataDirectories().getExportDir().isEmpty()) {
            throw new ParameterException("Export directory cannot be empty \\n" +
                    " Set data directory using command --export-dir || -ed <exportDirectoryPath>");
        }
        return configuration.getDataDirectories().getExportDir();
    }

    private void validateReportParameters() {

        if (parameters.getYear() == 0 && parameters.getQuarter().isEmpty() && parameters.getMonths() == 0) {
            throw new ParameterException("Year, month or quarter parameters cannot be empty.\n " +
                    "Available options: process --company <company-name> -year <yyyy> for yearly report.\n " +
                    "process --company <company-name> -year <yyyy> -quarter <q1> for quarterly report.\n " +
                    "process --company <company-name> -year <yyyy> -month <MM> for monthly report.\n");
        } else if (parameters.getYear() == 0) {
            throw new ParameterException("Year parameter cannot be empty");
        } else if (parameters.getMonths() != 0 && parameters.getMonths() < 0 && parameters.getMonths() > 12) {
            throw new ParameterException("Value for month cannot be less than 1 and greater than 12");
        } else if (parameters.getQuarter() != null && !isQuarterValid(parameters.getQuarter())) {
            throw new ParameterException("Unrecognized parameter for -quarter, " + parameters.getQuarter() + "\n " +
                    " Acceptable -quarter parameters <q1,q2,q3,q4>");
        }

    }

    private void validateAggregationType() {

        if (parameters.getAggregation().isEmpty()) {
            throw new ParameterException("Aggregation value cannot be empty");
        } else if (!isAggregationValid(parameters.getAggregation())) {
            throw new ParameterException("Unrecognized parameter for -aggregation " + parameters.getAggregation() + "\n" +
                    " Acceptable -aggregation parameters <store, invoice, receipt, payment>");
        }
    }

    public void doesCompanyExist(CompanyDataProcessor processor) {

        if(parameters.getCompanyName() == null){
            throw new ParameterException("Company name,in command report -company <companyName> cannot be blank");
        }else if (!processor.getCompanyService().findByName(parameters.getCompanyName()).isPresent()) {
            throw new ParameterException(String.format("Company with name %s, not found!", parameters.getCompanyName()));
        }
    }

    private boolean isAggregationValid(String aggregation) {
        aggregation = aggregation.toLowerCase();
        return aggregation.equals(ReportConstants.STORE_AGGREGATION ) ||
                aggregation.equals(ReportConstants.INVOICE_AGGREGATION ) ||
                aggregation.equals(ReportConstants.RECEIPT_AGGREGATION ) ||
                aggregation.equals(ReportConstants.PAYMENT_AGGREGATION );
    }


    private boolean isQuarterValid(String quarter) {
        quarter = quarter.toLowerCase();
        return quarter.equals("q1") || quarter.equals("q2") || quarter.equals("q3") || quarter.equals("q4");
    }
}
