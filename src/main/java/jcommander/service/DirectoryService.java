package jcommander.service;

import configuration.AppConfiguration;
import configuration.AppConfigurer;
import deserialization.XmlParser;
import jcommander.CommandLineParameters;

public class DirectoryService {

    private CommandLineParameters parameters;
    private AppConfiguration configuration;
    private XmlParser<AppConfiguration> parser;

    public DirectoryService(CommandLineParameters parameters) {
        this.parameters = parameters;
        configuration = AppConfigurer.getConfiguration();
        parser = new XmlParser<>(AppConfiguration.class);
    }

    public void setDirectories() {

        if (parameters.getImportDirectory() != null) {
            configuration.getDataDirectories().setImportDir(parameters.getImportDirectory());
            parser.marshall(configuration, AppConfigurer.getConfigFile());

        }
        if (parameters.getExportDirectory() != null) {

            configuration.getDataDirectories().setExportDir(parameters.getExportDirectory());
            parser.marshall(configuration, AppConfigurer.getConfigFile());
        }
    }


}
