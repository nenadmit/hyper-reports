package jcommander;

import database.CompanyDataParser;
import database.CompanyDataProcessor;
import deserialization.XmlParser;
import deserialization.pojo.company.Company;
import jcommander.service.DirectoryService;
import jcommander.service.ParameterValidator;
import jcommander.service.ReportCreator;
import report_engine.XlsxParser;
import sftp.SftpDownloader;


public class CommandLineService {

    private CommandLineParameters parameters;
    private CompanyDataProcessor processor;
    private SftpDownloader downloader;
    private XmlParser<Company> companyParser;
    private XlsxParser xlsxParser;

    public CommandLineService(CommandLineParameters parameters,
                              CompanyDataProcessor processor,
                              SftpDownloader downloader,
                              XmlParser<Company> companyParser,
                              XlsxParser xlsxParser) {
        this.parameters = parameters;
        this.processor = processor;
        this.downloader = downloader;
        this.companyParser = companyParser;
        this.xlsxParser = xlsxParser;
    }


    public ParameterValidator validationService(){
        return new ParameterValidator(parameters,processor);
    }

    public ReportCreator reportService(){
        return new ReportCreator(parameters,processor,xlsxParser);
    }

    public DirectoryService directoryService(){
        return new DirectoryService(parameters);
    }

    public CompanyDataParser getDataProcessor(){
        return new CompanyDataParser(processor,companyParser);
    }

    public SftpDownloader downloader(){
        return downloader;
    }

}
