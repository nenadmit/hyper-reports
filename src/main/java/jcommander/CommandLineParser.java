package jcommander;

import com.beust.jcommander.JCommander;
import database.FlywayConfiguration;
import org.flywaydb.core.Flyway;


public class CommandLineParser {

    private CommandLineService service;
    private CommandLineParameters parameters;

    public CommandLineParser(CommandLineService service, CommandLineParameters parameters) {
        this.service = service;
        this.parameters = parameters;
    }

    public void parse(String[] args) {

        JCommander commander = new JCommander(parameters);
        commander.setProgramName("hyper-reports");

        commander.parse(args);

        if (parameters.isHelp()) {
            commander.usage();
            return;
        }

        if(parameters.isConfig()){
            service.directoryService().setDirectories();
            return;
        }

        if(parameters.isDownload()){
            String importDir = service.validationService().validateImportDirectory();
            service.downloader().downloadFiles(importDir);
            return;
        }

        if(parameters.isProcess()){

            Flyway flyway = FlywayConfiguration.getFlyway();
            flyway.migrate();

            if(parameters.isProcessOnly()){
                String  importDir = service.validationService().validateImportDirectory();
                service.getDataProcessor().process(importDir);
            }else{
                String  importDir = service.validationService().validateImportDirectory();
                service.downloader().downloadFiles(importDir);
                service.getDataProcessor().process(importDir);
            }


            return;
        }

        if(parameters.isReport()){
            service.validationService().validateExportDirectory();
            service.validationService().validateParameter();
            service.reportService().createReport();

        }


    }


}
