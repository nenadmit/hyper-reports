package deserialization;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SchemaValidation {

    public static final String REPORT_SCHEMA = "schemas/company-report.xsd";
    public static final String CONFIG_SCHEMA = "schemas/configuration.xsd";
    private final Logger logger = Logger.getLogger(this.getClass().getName());


    public boolean validateXmlFile(File xmlFile,String schema){

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA,schema);
            Schema schema1 = schemaFactory.newSchema(new File(schema));
            Validator validator = schema1.newValidator();
            validator.validate(new StreamSource(xmlFile));
        } catch (SAXException e) {
            return false;
        } catch (IOException e) {
           logger.log(Level.SEVERE,e.getMessage());
        }
        return true;
    }
}
