package deserialization;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XmlParser<T> {

    private JAXBContext context;
    private Unmarshaller unmarshaller;
    private Marshaller marshaller;
    private final Logger logger = Logger.getLogger(this.getClass().getName());


    /**
     * JAXBContext.newIstance() requires .class as an argument
     * wont work with generic T, need to inject it via constructor
     *
     */

    public XmlParser(Class classPath){

        try{
            context = JAXBContext.newInstance(classPath);
            unmarshaller = context.createUnmarshaller();
            marshaller = context.createMarshaller();
        }catch (Exception e){
            logger.log(Level.SEVERE,e.getMessage());
        }
    }

    public T parse(File file) {

        try {
            return (T) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            logger.log(Level.SEVERE,e.getMessage());
        }
        return null;

    }

    public T parse(InputStream inputStream) throws JAXBException{

        T t = null;
        try{
            t = (T) unmarshaller.unmarshal(inputStream);
        }catch (JAXBException e){
            logger.log(Level.SEVERE,e.getMessage());
        }finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.log(Level.SEVERE,e.getMessage());
            }
        }
        return  t;

    }

    public void marshall(T t, File file){
        try {
            marshaller.marshal(t,file);
        } catch (JAXBException e) {
            logger.log(Level.SEVERE,e.getMessage());
        }
    }

    public T validateAndParse(File file, String schemaFile) {


        SchemaValidation validation = new SchemaValidation();

        if(validation.validateXmlFile(file,schemaFile)){
            try {
                return (T) unmarshaller.unmarshal(file);
            } catch (JAXBException e) {
                logger.log(Level.SEVERE,e.getMessage());
            }
        }else{
            logger.log(Level.WARNING,file.getName() + " is not in the right format!");
        }

        return null;
    }


}
