import com.beust.jcommander.ParameterException;
import database.CompanyDataProcessor;
import database.repositoryy.v2.RepositoryImpl;
import database.service.implementation.*;
import database.service.interfaces.*;
import deserialization.XmlParser;
import deserialization.pojo.company.*;
import jcommander.CommandLineParameters;
import jcommander.CommandLineParser;
import jcommander.CommandLineService;
import report_engine.XlsxParser;
import sftp.SftpDownloader;
import sftp.SftpDownloaderConfigurer;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class HyperReportsApp {

    private final Logger logger = Logger.getLogger("HyperReportsApp");

    private final CommandLineParameters parameters = new CommandLineParameters();

    public void run(String[] args){

        CommandLineService service = new CommandLineService(parameters,
                getDataProcessor(),
                getDownloader(),
                getCompanyParser(),
                getXlsxParser());

        CommandLineParser parser = new CommandLineParser(service,parameters);
        try{
            parser.parse(args);
        }catch (ParameterException e){
            logger.log(Level.WARNING,e.getMessage());
        }

    }

    private XmlParser<Company> getCompanyParser(){
        return new XmlParser<>(Company.class);
    }

    private XlsxParser getXlsxParser(){
        return new XlsxParser();
    }

    private SftpDownloader getDownloader(){
        SftpDownloaderConfigurer downloaderConfigurer = new SftpDownloaderConfigurer();
        return new SftpDownloader(downloaderConfigurer, getDataProcessor());
    }

    private CompanyDataProcessor getDataProcessor() {
        CompanyService companyService = new CompanyServiceImpl(new RepositoryImpl(Company.class));
        StoreService storeService = new StoreServiceImpl(new RepositoryImpl(Store.class));
        CustomerService customerService = new CustomerServiceImpl(new RepositoryImpl(Customer.class));
        CardService cardService = new CardServiceImpl(new RepositoryImpl<>(Card.class));
        ReceiptService receiptService = new ReceiptServiceImpl(new RepositoryImpl<>(Receipt.class));
        InvoiceService invoiceService = new InvoiceServiceImpl(new RepositoryImpl<>(Invoice.class));
        XmlCompanyFileService xmlCompanyFileService = new XmlCompanyFileService(new RepositoryImpl<>(XmlCompanyReport.class));

        return new CompanyDataProcessor(
                companyService, storeService,
                customerService, cardService,
                receiptService, invoiceService,
                xmlCompanyFileService);


    }
}
